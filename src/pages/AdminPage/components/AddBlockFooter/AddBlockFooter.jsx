import { connect } from 'react-redux';
import { PureComponent } from 'react';

import './AddBlockFooter.scss';

import Input from '../../../../components/Input/Input';
import Button from '../../../../components/Button/Button';
import { addBlockFooter } from '../../../../actionCreators/actionCreators';

class AddBlockFooter extends PureComponent {
  state = {
    title: '',
    text: '',
  };

  handleClick = () => {
    const {addBlockFooter} = this.props;
    const {title, text} = this.state;
    const blockFooter = {
      title,
      text,
    };
  
    addBlockFooter(blockFooter);    
  }

  handleChangeTitle = (e) => {
    this.setState({title: e.target.value})
  };

  handleChangeText = (e) => {
    this.setState({text: e.target.value})
  };

  render() {
    const {title, text} = this.state;

    return (
      <div className="wrapper-add">
        <Input className="wrapper-add__item " placeholder="title" onChange={this.handleChangeTitle} value={title} maxLength="15" />
        <textarea rows="5" cols="31" maxLength="60" onChange={this.handleChangeText} value={text} placeholder="text"/>
        <Button text="добавить\ редактировать" onClick={this.handleClick} />
      </div>   
    );
  }  
}

const mapStateToProps = ({ admin }) => ({
  blocksFooter: admin.blocksFooter,
});

const mapDispatchToProps = (dispatch) => ({
  addBlockFooter: (blockFooter) => dispatch(addBlockFooter(blockFooter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBlockFooter);