import { connect } from 'react-redux';
import { PureComponent } from 'react';

import './DeleteBlock.scss';

import Button from '../../../../components/Button/Button';
import { deleteDirection } from '../../../../actionCreators/actionCreators';


class DeleteBlock extends PureComponent {
  state = {
    country: '',
  };

  handleClick = () => {
    const {deleteDirection} = this.props;

    const {country} = this.state;
    const direction = {
      country,
      
    };

    deleteDirection(direction);
  }

  handleChangeCountry = (e) => {
    this.setState({country: e.target.value})
  }; 

  render() {

    const {country} = this.state;
    
    return (
      <div className="wrapper-delete">
        <label>Выберите country: </label>
        <select className="wrapper-delete__list" value={country} onChange={this.handleChangeCountry}>
          <option className="wrapper-delete__list-item" value="Greece">Greece</option>
          <option className="wrapper-delete__list-item" value="Spain">Spain</option>
          <option className="wrapper-delete__list-item" value="Canada">Canada</option>
          <option className="wrapper-delete__list-item" value="Africa">Africa</option>
          <option className="wrapper-delete__list-item" value="India">India</option>
          <option className="wrapper-delete__list-item" value="Vietnam">Vietnam</option>
          <option className="wrapper-delete__list-item" value="Egypt">Egypt</option>
        </select>
        <Button text="удалить"  onClick={this.handleClick} />
      </div>   
    );
  }
}

const mapStateToProps = ({ directions }) => ({
  directions: directions.directions,
});

const mapDispatchToProps = (dispatch) => ({
  deleteDirection: (direction) => dispatch(deleteDirection(direction)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteBlock);