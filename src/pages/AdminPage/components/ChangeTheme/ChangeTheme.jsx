import { connect } from 'react-redux';
import { PureComponent } from 'react';

import './ChangeTheme.scss';

import { changeTheme } from '../../../../actionCreators/actionCreators';

class ChangeTheme extends PureComponent {

  handleChange = (event) => {
    const {changeTheme} = this.props;
    changeTheme(event.target.value); 
  }

  render() {
    const {theme} = this.props;
    
    return (
      <div className="wrapper-change-theme">
        <label>Выберите тему: </label>
        <select className="wrapper-change-theme__list" value={theme} onChange={this.handleChange}>
          <option className="wrapper-change-theme_item" value="yellow">yellow</option>
          <option className="wrapper-change-theme_item" value="orange">orange</option>
        </select>
      </div>   
    );
  }
}

const mapStateToProps = ({ admin }) => ({
  theme: admin.theme,
});

const mapDispatchToProps = (dispatch) => ({
  changeTheme: (theme) => dispatch(changeTheme(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeTheme);