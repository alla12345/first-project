import { connect } from 'react-redux';
import { PureComponent } from 'react';

import './AddBlock.scss';

import Input from '../../../../components/Input/Input';
import Button from '../../../../components/Button/Button';
import { addDirection } from '../../../../actionCreators/actionCreators';
import greece from '../../../../assets/img/DirectionsContainer/greece.jpeg';
import spain from '../../../../assets/img/DirectionsContainer/spain.jpeg';
import canada from '../../../../assets/img/DirectionsContainer/canada.jpeg';
import africa from '../../../../assets/img/DirectionsContainer/africa.jpeg';
import india from '../../../../assets/img/DirectionsContainer/india.jpg';
import vietnam from '../../../../assets/img/DirectionsContainer/vietnam.jpg';
import egypt from '../../../../assets/img/DirectionsContainer/egypt.jpg';

class AddBlock extends PureComponent {
  state = {
    country: '',
    img: '',
    title: '',
    text: '',
  };

  handleClick = () => {
    const {addDirection} = this.props;
    const {country, img, title, text} = this.state;
    const direction = {
      country,
      img,
      title,
      text,
    };
    addDirection(direction);
  }

  handleChangeCountry = (e) => {
    this.setState({country: e.target.value})
  };

  handleChangeImg = (e) => {
    this.setState({img: e.target.value})
  };

  handleChangeTitle = (e) => {
    this.setState({title: e.target.value})
  };

  handleChangeText = (e) => {
    this.setState({text: e.target.value})
  };

  render() {
    const {country, img, title, text} = this.state;

    return (
      <div className="wrapper-add">
        <label>Выберите country: </label>
        <select className="wrapper-add__list" value={country} onChange={this.handleChangeCountry}>
          <option className="wrapper-add__list-item" value="Greece">Greece</option>
          <option className="wrapper-add__list-item" value="Spain">Spain</option>
          <option className="wrapper-add__list-item" value="Canada">Canada</option>
          <option className="wrapper-add__list-item" value="Africa">Africa</option>
          <option className="wrapper-add__list-item" value="India">India</option>
          <option className="wrapper-add__list-item" value="Vietnam">Vietnam</option>
          <option className="wrapper-add__list-item" value="Egypt">Egypt</option>
        </select>
        <Input className="wrapper-add__item" placeholder="title" onChange={this.handleChangeTitle} value={title}/>
        <label>Выберите picture : </label>
        <select className="wrapper-add__list" value={img} onChange={this.handleChangeImg}>
          <option className="wrapper-add__list-item" value={greece}>greece</option>
          <option className="wrapper-add__list-item" value={spain}>spain</option>
          <option className="wrapper-add__list-item" value={canada}>canada</option>
          <option className="wrapper-add__list-item" value={africa}>africa</option>
          <option className="wrapper-add__list-item" value={india}>india</option>
          <option className="wrapper-add__list-item" value={vietnam}>vietnam</option>
          <option className="wrapper-add__list-item" value={egypt}>egypt</option>
        </select>
        <Input className="wrapper-add__item" placeholder="text"onChange={this.handleChangeText} value={text}/>
        <Button text="добавить\ редактировать" onClick={this.handleClick} disabled={!country || !img || !title || !text} />
      </div>   
    );
  }  
}

const mapStateToProps = ({ directions }) => ({
  directions: directions.directions,
});

const mapDispatchToProps = (dispatch) => ({
  addDirection: (direction) => dispatch(addDirection(direction)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBlock);