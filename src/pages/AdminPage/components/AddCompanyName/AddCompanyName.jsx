import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { addCompanyName } from '../../../../actionCreators/actionCreators';

import './AddCompanyName.scss'; 

import Input from '../../../../components/Input/Input';
import Button from '../../../../components/Button/Button';

class AddCompanyName extends PureComponent {
  constructor(props) {        
    super(props);
    this.state = {
      companyName: props.companyName,
    }
  }

  handleClick = () => {
    const {addCompanyName} = this.props;
    const {companyName} = this.state;
    addCompanyName(companyName);
  }

  handleChange = (e) => {
    this.setState({companyName: e.target.value})
  };

  render() {
    const {companyName} = this.state;

    return (
      <div className="wrapper-name">
        <Input className="wrapper-name__item" onChange={this.handleChange} value={companyName} />
        <Button text="изменить" onClick={this.handleClick} disabled={!companyName || (companyName === this.props.companyName)} />
      </div>
    )
  }
}

const mapStateToProps = ({admin}) => ({
  companyName: admin.companyName,
});

const mapDispatchToProps = (dispatch) => ({
  addCompanyName: companyName => dispatch(addCompanyName(companyName)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddCompanyName);
