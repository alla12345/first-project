import { connect } from 'react-redux';
import { PureComponent } from 'react';

import './DeleteBlockFooter.scss';

import Button from '../../../../components/Button/Button';
import Input from '../../../../components/Input/Input';
import { deleteBlockFooter } from '../../../../actionCreators/actionCreators';


class DeleteBlockFooter extends PureComponent {
  state = {
    title: '',
  };

  handleClick = () => {
    const {deleteBlockFooter} = this.props;

    const {title} = this.state;
    const blockFooter = {
      title,
      
    };

    deleteBlockFooter(blockFooter);
  }

  handleChangeTitle = (e) => {
    this.setState({title: e.target.value})
  };

  render() {

    const {title} = this.state;
    
    return (
      <div className="wrapper-delete-block">
        <Input className="wrapper-delete-block__item" placeholder="title" onChange={this.handleChangeTitle} value={title}/>
        <Button text="удалить"  onClick={this.handleClick} />
      </div>   
    );
  }
}

const mapStateToProps = ({ admin }) => ({
  blocksFooter: admin.blocksFooter,
});

const mapDispatchToProps = (dispatch) => ({
  deleteBlockFooter: (blockFooter) => dispatch(deleteBlockFooter(blockFooter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteBlockFooter); 