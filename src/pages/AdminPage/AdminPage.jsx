import { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import './AdminPage.scss'; 

import Button from '../../components/Button/Button';
import AddBlock from './components/AddBlock/AddBlock';
import DeleteBlock from './components/DeleteBlock/DeleteBlock'; 
import AddCompanyName from './components/AddCompanyName/AddCompanyName';
import ChangeTheme from './components/ChangeTheme/ChangeTheme';
import AddBlockFooter from './components/AddBlockFooter/AddBlockFooter';
import DeleteBlockFooter from './components/DeleteBlockFooter/DeleteBlockFooter';

class AdminPage extends PureComponent {
 
  handleClick = () => {
    this.props.history.push('/home');
  }

  render() {  
    return (
      <div className="wrapper-admin">
        <div className="wrapper-admin__exercise">
          <div className="wrapper-admin__info" >oсновное задание</div>
          <AddBlock />
          <DeleteBlock />
          <Button text="на главную" onClick={this.handleClick} />
        </div>
        <div className="wrapper-admin__exercise">  
          <div className="wrapper-admin__info" >доп. задание</div>
          <AddCompanyName />
          <AddBlockFooter /> 
          <DeleteBlockFooter />
          <ChangeTheme />    
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ directions }) => ({
  directions: directions.directions,
});

export default connect(mapStateToProps)(withRouter(AdminPage));
