import { PureComponent } from 'react';
import { Fragment } from 'react';

import WelcomeContainer from './components/WelcomeContainer/WelcomeContainer';
import DirectionsContainer from './components/DirectionsContainer/DirectionsContainer';
import PlacesContainer from './components/PlacesContainer/PlacesContainer';
import ContactContainer from './components/ContactContainer/ContactContainer';

export default class HomePage extends PureComponent {

  render() {    
    return (
      <Fragment>
        <WelcomeContainer />
        <DirectionsContainer />
        <PlacesContainer />
        <ContactContainer />      
      </Fragment>
    )
  }
}

