import { PureComponent } from 'react';
import { connect } from 'react-redux';
import withYellowTheme from '../../../../hocs/withYellowTheme';

import './DirectionsContainer.scss';

import Button from '../../../../components/Button/Button';
import Line from '../../../../components/Line/Line';
import DirectionsCards from './DirectionsCards/DirectionsCards';

class DirectionsContainer extends PureComponent {
  state = { 
    isShowMore: false,
  }

  handleClick = () => {
    this.setState(({ isShowMore }) => ({isShowMore: !isShowMore}));
  }

	render() {
    const {directions, isYellowTheme} = this.props;
    const {isShowMore} = this.state;
    const showedDirectionCards = isShowMore ? directions : directions.slice(0, 4);
   
	  return (
		  <div className="wrapper-directions">		  
				<div className="wrapper-directions__background">	  
					<div className="wrapper-directions__title">
            <h2 className={`wrapper-directions__info ${isYellowTheme ? 'wrapper-directions__info_yellow' : ''}`}>Горячие Направления</h2>
						<Line />
						<div className="wrapper-directions__text">Первое место для
							поиска экологически чистого отдыха
            </div>
					</div>	
				</div> 
  
				<div className="wrapper-directions__cards">
					{showedDirectionCards.map((item) => (
						<DirectionsCards
							key={item.country}
							img={item.img}
							title={item.title}
							text={item.text} 
						/> 
        	))}
				</div>  
				
				<div className="wrapper-directions__button">
          <Button text={isShowMore ? "свернуть" : "показать больше"} onClick={this.handleClick} />
				</div>	
				<hr/>	
		  </div>
	  )
  }
}

const mapStateToProps = ({ directions }) => ({
  directions: directions.directions,
});


export default withYellowTheme(connect(mapStateToProps)(DirectionsContainer));
