import { PureComponent, Fragment } from 'react';
import withYellowTheme from '../../../../hocs/withYellowTheme';

import './WelcomeContainer.scss';

import holiday from '../../../../assets/img/WelcomeContainer/icon/holiday.svg';
import guide from '../../../../assets/img/WelcomeContainer/icon/guide.svg';
import tour from '../../../../assets/img/WelcomeContainer/icon/tour.svg';
import vacation from '../../../../assets/img/WelcomeContainer/icon/vacation.svg';

import holidayYellow from '../../../../assets/img/Yellow/holidayYellow.svg';
import guideYellow from '../../../../assets/img/Yellow/guideYellow.svg';
import tourYellow from '../../../../assets/img/Yellow/tourYellow.svg';
import vacationYellow from '../../../../assets/img/Yellow/vacationYellow.svg';

import WelcomeCards from './WelcomeCards/WelcomeCards';
import WelcomeTitle from '../../../../components/WelcomeTitle/WelcomeTitle';

class WelcomeContainer extends PureComponent {
  render() {
    const { isYellowTheme } = this.props;
    const arrayWelcome = [
      { id: 0, icon: holiday, iconYellow: holidayYellow, title: "Типы Праздников", text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam." },
      { id: 1, icon: guide, iconYellow: guideYellow, title: "Путеводители", text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam." },
      { id: 2, icon: tour, iconYellow: tourYellow, title: "Экстремальные Туры", text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam." },
      { id: 3, icon: vacation, iconYellow: vacationYellow, title: "Отпуск Мечты", text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam." },
    ];
  
    return (
      <Fragment>   
        <WelcomeTitle />
        <div className="wrapper-welcome-row">
          {arrayWelcome.map((item) => (
            <WelcomeCards
              key={item.id}
              icon={isYellowTheme ? item.iconYellow : item.icon}
              title={item.title}
              text={item.text} 
            /> 
          ))}
        </div>        
      </Fragment>
    )
  }
}

export default withYellowTheme(WelcomeContainer); 
