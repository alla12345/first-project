import React, { Component } from 'react';
import withYellowTheme from '../../../../../hocs/withYellowTheme.jsx';

import './WelcomeCards.scss';

import Line from '../../../../../components/Line/Line';
class WelcomeCards extends Component {

  render() {
    const { isYellowTheme, icon, title, text } = this.props;

    return (
      <div className="cards-wrap">
        <img src={icon} className={`cards-wrap__icon ${isYellowTheme ? 'cards-wrap__icon_yellow' : ''}`} alt="icon" ></img>
        <div className="cards-wrap__title">{title}</div>
        <div className="cards-wrap__text">{text}</div>
        <Line />
      </div>    
    );
  } 
}

  export default withYellowTheme(WelcomeCards);
