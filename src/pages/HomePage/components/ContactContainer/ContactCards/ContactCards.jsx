import React, { Component } from 'react';
import withYellowTheme from '../../../../../hocs/withYellowTheme';

import './ContactCards.scss';

class ContactCards extends Component { 
  render() {
    const { isYellowTheme, title, number } = this.props;

    return (
      <div className="contact-cards-wrap">
        <div className="contact-cards-wrap__title">{title}</div>
        <div className={`contact-cards-wrap__text ${isYellowTheme ? 'contact-cards-wrap__text_yellow' : ''}`}>{number}</div>
      </div>    
    );
  }
}

export default withYellowTheme(ContactCards);
