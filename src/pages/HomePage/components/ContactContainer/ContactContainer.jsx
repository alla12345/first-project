import { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import withYellowTheme from '../../../../hocs/withYellowTheme';

import './ContactContainer.scss';
import Button from '../../../../components/Button/Button'
import Line from '../../../../components/Line/Line'
import ContactCards from './ContactCards/ContactCards';

class ContactContainer extends PureComponent {
  handleClick = () => {
    this.props.history.push('/contacts');
  }

  render() {
	  const arrayContact = [
      { id: 0, title: "ГОРЯЧИЕ НАПРАВЛЕНИЯ", number: "37" },
      { id: 1, title: "ДОВОЛЬНЫЕ КЛИЕНТЫ", number: "677" },
      { id: 2, title: "чашки кофе", number: "87" },
      { id: 3, title: "ПОСЕТИЛИ СТРАНЫ", number: "107" },		
	  ];

    const {isYellowTheme} = this.props;

    return (
      <div className="wrapper-contact">
		  	<div className="wrapper-contact__background">
          <div className="wrapper-contact__title">
            <h2 className={`wrapper-contact__info ${isYellowTheme ? 'wrapper-contact__info_yellow' : ''}`}>Помогая Мечтам Исполняться</h2>
            <Line />
            <div className="wrapper-contact__text">Вы можете получить от нас столько помощи, сколько захотите. Тебе решать. Наконец, вы должны знать, 
              что мы относимся к местным жителям с уважением и справедливостью. Это окупается загрузкой ведра, 
              потому что заботливые местные жители позволяют ближе к их культуре, их людям и их природе.
              Что хорошо для них и хорошо для вас. Кроме того, если вы хотите, при бронировании отпуска 
              мы оплатим однодневную поездку для малообеспеченного ребенка из развивающейся страны. в игровой парк,
              гору или музей и т. д.
              Мы называем это ответственным туризмом.
            </div>
            <Button text="связаться" onClick={this.handleClick} />
          </div>
        </div>
        <div className="wrapper-contact__cards">
          {arrayContact.map((item) => (
            <ContactCards
              key={item.id}
              title={item.title}
              number={item.number} 
            /> 
          ))}	
        </div> 
		  </div>
    )
  }
}

export default withYellowTheme(withRouter(ContactContainer));
