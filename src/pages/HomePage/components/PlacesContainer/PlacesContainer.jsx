import { PureComponent } from 'react';

import './PlacesContainer.scss';

import morning from '../../../../assets/img/PlacesContainer/morning.jpeg'
import waterfall from '../../../../assets/img/PlacesContainer/waterfall.jpeg';
import mountains from '../../../../assets/img/PlacesContainer/mountains.jpeg';
import town from '../../../../assets/img/PlacesContainer/town.jpeg';
import road from '../../../../assets/img/PlacesContainer/road.jpeg';
import fog from '../../../../assets/img/PlacesContainer/fog.jpeg';
import river from '../../../../assets/img/PlacesContainer/river.jpeg';

import Line from '../../../../components/Line/Line'
import Form from '../../../../components/Form/Form';

export default class PlacesContainer extends PureComponent {
  render() {
    const placesPhoto = {
      morning,
      waterfall,
      mountains,
      town,
      road, 
      fog,		
      river,	
    };

    return (
     	<div className="wrapper-places">
        <div className="wrapper-places__title-photos">
          <div className="wrapper-places__title">
            <h3>Удивительные Места</h3>
            <Line />
            <div className="wrapper-places__text">Lorem ipsum dolor
              sit amet, consectetur adipiscing elit nullam nunc
              justo sagittis suscipit ultrices.
            </div>
          </div>

          <div className="wrapper-places__photos">
            <div className="wrapper-places__photos-column"> 
              <img className="wrapper-places__photo" src={placesPhoto.morning} alt="" />	
              <img className="wrapper-places__photo" src={placesPhoto.waterfall} alt="" />								
            </div>
            <div className="wrapper-places__photos-column wrapper-places__photos-column-center">
              <img className="wrapper-places__photo-center" src={placesPhoto.mountains} alt="" />
              <img className="wrapper-places__photo-center" src={placesPhoto.town} alt="" />
              <img className="wrapper-places__photo-center" src={placesPhoto.road} alt="" /> 
            </div>
            <div className="wrapper-places__photos-column wrapper-places__photos-column-wide">
              <img className="wrapper-places__photo" src={placesPhoto.fog} alt="" />
              <img className="wrapper-places__photo" src={placesPhoto.river} alt="" />
            </div>
          </div>
        </div>

        <Form />
      </div>
    )
  }
}
