import { PureComponent, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import './ContactsPage.scss';

import Button from '../../components/Button/Button';
import WelcomeTitle from '../../components/WelcomeTitle/WelcomeTitle';
import Form from '../../components/Form/Form';
import DirectionsPhoto from '../ContactsPage/components/DirectionsPhoto/DirectionsPhoto';
import DirectionsInfo from '../ContactsPage/components/DirectionsInfo/DirectionsInfo';

class ContactsPage extends PureComponent {
  handleClick = () => {
    this.props.history.push('/home');
  }

  render() {  
    const {directions, match} = this.props;
    const id = match.params.id;
    const direction = directions.find(item => String(item.id) === String(id));
 
    return (
      <Fragment>
        <WelcomeTitle />
        <div className="wrapper-contacts">
          <Form />  

          <div className="wrapper-contacts__contacts">
            <p>Адрес компании:  ул. Строителей, 12, Ленинград</p>
            <p>Телефоны:
            A1 +375-29-333-33-33, МТС +375-29-111-11-11 </p>  
          </div>

          <div className="wrapper-contacts__photo">
            {directions.map((item) => (  
              <DirectionsPhoto
                key={item.id}
                img={item.img}
                id={item.id}
                country={item.country}
              />
          	))}
				  </div>
          {direction &&
            <DirectionsInfo title={direction.title} text={direction.text} />
          }

          <Button text="на главную" onClick={this.handleClick} />
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ directions }) => ({
  directions: directions.directions,
});

export default connect(mapStateToProps)(withRouter(ContactsPage));
