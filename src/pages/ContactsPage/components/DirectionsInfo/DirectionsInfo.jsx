import './DirectionsInfo.scss';

export default function DirectionsInfo({title, text}) {
  return (
    <div className="directions-info-wrap">
      <div className="directions-info-wrap__title">{title}</div>
      <div className="directions-info-wrap__text">{text}</div>
    </div>    
  );
}