import { Link } from 'react-router-dom';

import './DirectionsPhoto.scss';

export default function DirectionsPhoto({img, id, country}) {
  return (
    <div className="directions-photo-wrap">
      <Link to={`/contacts/${id}/${country}`}>
        <img src={img} className="directions-photo-wrap__img" alt=""></img>
      </Link>
    </div>    
  );
}
