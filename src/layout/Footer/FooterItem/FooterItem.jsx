import './FooterItem.scss';

export default function FooterItem({title, text}) {
  return (
    <div className="footer-item">
      <h4 className="footer-item__title">{title}</h4>
      <div className="footer-item__text">{text}</div>
    </div>    
  );
}