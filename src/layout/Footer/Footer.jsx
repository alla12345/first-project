import { PureComponent } from 'react';
import { connect } from 'react-redux';

import './Footer.scss';

import FooterItem from './FooterItem/FooterItem';

class Footer extends PureComponent {
  render() {
    const {companyName, blocksFooter} = this.props;
    // const showedBlockFooter = blocksFooter.length < 3 ? blocksFooter : blocksFooter.slice(0, 3); 
  	return (
      <div className="footer">
      	<div className="footer__items">
          {blocksFooter.map((item, index) => (
            <FooterItem 
              key={item.title + index}
              title={item.title}              
              text={item.text} 
            /> 
			    ))}	
        <hr/> 
        <h4 className="footer__company-name">Туристическая Компания {companyName}</h4>
      </div>      
    </div> 
    );
  }
}

const mapStateToProps = ({admin}) => ({
  companyName: admin.companyName,
  blocksFooter: admin.blocksFooter,
})

export default connect(mapStateToProps)(Footer);
