import { PureComponent } from 'react';
import { connect } from 'react-redux';

import './Header.scss';

class Header extends PureComponent {

  render() {
    const {companyName} = this.props;
    
    return (
      <div className="wrapper-header"> 
        <h5 className="wrapper-header__company-name">Туристическое АГЕНТСТВО {companyName}</h5>
      </div>  
    );
  }  
}

const mapStateToProps = ({ admin }) => ({
  companyName: admin.companyName,
})

export default connect(mapStateToProps)(Header);