import React, { Component } from 'react';
import { connect } from 'react-redux';

const withYellowTheme = (WrappedComponent) => {
  class HOC extends Component {
    render() {
      const {theme} = this.props;
      return (
        <WrappedComponent
          {...this.props}
          isYellowTheme={theme === 'yellow'}
        />
      );
    }
  }
  return connect(mapStateToProps)(HOC);
};

const mapStateToProps = ({ admin }) => ({
  theme: admin.theme,
});

export default withYellowTheme;
