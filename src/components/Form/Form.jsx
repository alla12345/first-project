import { PureComponent } from 'react';

import './Form.scss';

import Button from '../Button/Button';
import Input from '../Input/Input';

export default class Form extends PureComponent {
  state = {
    isLoading: false,
    isShowForm: true,
    email:'',
    name:'',
    error: '',
  };

  handleChangeName = (e) => {
    this.setState({name: e.target.value})
  };

  handleChangeEmail = (e) => {
    this.setState({email: e.target.value})
  };

  handleClick = async () => {
    this.setState({isLoading: true}); 
    try {
    await fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
      }),
    })
    this.setState({isLoading: false, isShowForm: false});

    } catch (err) {
    this.setState({isLoading: false});
    }
  }

  render() {
    const {isShowForm, email, name, isLoading } = this.state;

    return (
      <div className="form">
        {isShowForm ? 
          (<div className="form__wrap">
            <Input  placeholder="Enter your Name" onChange={this.handleChangeName} value={name} />
            <Input  placeholder="Enter your email" onChange={this.handleChangeEmail} value={email} />
            <Button text="представить" onClick={this.handleClick} disabled={!name || !email || isLoading} /> 
          </div>)
          : 'мы с вами свяжемся'
        }
      </div> 
    )
  }
}
