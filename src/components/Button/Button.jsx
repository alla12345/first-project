import React, { Component } from 'react';
import withYellowTheme from '../../hocs/withYellowTheme';

import './Button.scss';

class Button extends Component {
  render() {
    const { isYellowTheme, className, text, onClick, disabled } = this.props;
    const themeClassname = isYellowTheme ? 'button__yellow' : 'button__orange';
    
    return (
      
      <button className={`button ${className}  ${themeClassname}`}
       onClick={onClick} disabled={disabled}>{text}</button>
    );
  }
}

export default withYellowTheme(Button);
