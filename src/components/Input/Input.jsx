import './Input.scss';
 
export default function Input({placeholder, value, onChange, maxLength, className = ''}) {
  return (
    <input className={`input ${className}`} placeholder={placeholder} value={value} onChange={onChange}  maxLength={maxLength}></input>
  );
}
