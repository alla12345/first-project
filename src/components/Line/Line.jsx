import withYellowTheme from '../../hocs/withYellowTheme';

import './Line.scss';

const Line = ({isYellowTheme}) => {    
    return (
      <div className={`line ${isYellowTheme ? 'line_yellow' : ''}`}></div>    
    );
  } 

export default withYellowTheme(Line);
