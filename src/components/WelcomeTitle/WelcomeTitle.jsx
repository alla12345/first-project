import { connect } from 'react-redux';

import './WelcomeTitle.scss';
import ball from '../../assets/img/WelcomeTitle/ball.png';
import Button from '../Button/Button'
import { openPopup } from '../../actionCreators/actionCreators';

const WelcomeTitle = ({openPopup}) => {
    return (
      <div className="wrapper-welcome">
        <div className="wrapper-welcome__wrap">
          <div className="wrapper-welcome__welcome">
              <img src={ball} className="wrapper-welcome__ball" alt=""></img>
              <h6>пришло время для великого</h6>
              <h1>Приключение</h1>  
              <Button text="начать сейчас" className="button__welcome" onClick={openPopup} />
          </div> 
        </div>    	
      </div>        
    )
  }


const mapDispatchToProps = dispatch => ({
  openPopup: () => dispatch(openPopup())
});

export default connect(null, mapDispatchToProps)(WelcomeTitle);
