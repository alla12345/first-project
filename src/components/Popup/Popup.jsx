import { connect } from 'react-redux';

import './Popup.scss';

import Cross from '../Cross/Cross';
import { closePopup } from '../../actionCreators/actionCreators';

// function Popup({isPopupOpen, closePopup}) {
const Popup = ({isPopupOpen, closePopup}) => {
    if (!isPopupOpen) {
      return null;
    }

  	return ( 
      <div className="popup">       
        <div className="popup__content">
          <Cross className="popup__cross" onClick={closePopup} />
          <div className="popup__text">Это заготовка попапа</div> 
        </div>
      </div>				
    );
  }

const mapStateToProps = ({popup}) => ({
  isPopupOpen: popup.isPopupOpen,
});

const mapDispatchToProps = dispatch => ({
  closePopup: () => dispatch(closePopup())
});

export default connect(mapStateToProps, mapDispatchToProps)(Popup);