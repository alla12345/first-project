import { combineReducers } from 'redux';
import popupReducer from './popupReducer';
import directionsReducer from './directionsReducer';
import adminReducer from './adminReducer';

export default combineReducers({
  popup: popupReducer,
  directions: directionsReducer,
  admin: adminReducer,
});
