const initState = {
  isLoading: false,
  directions: [],
}

function reducer(state = initState, action) {
  switch(action.type) {
    case 'SUCCES_FETCH_DIRECTIONS': {
      return { 
        ...state,
        isLoading: false,
        directions: action.payload.directions,  
      }
    }
    case 'START_FETCH_DIRECTIONS': {
      return { ...state, isLoading: true }
    }
    case 'FAILED_FETCH_DIRECTIONS': {
      return { ...state, isLoading: false }
    }
    case 'ADD_DIRECTION': {
      let newDirections = [...state.directions];
      let directionIndex = newDirections.findIndex(direction => direction.country === action.payload.direction.country);
  
      if (directionIndex !== -1) {
        newDirections.splice(directionIndex, 1, action.payload.direction);
      } else {
        newDirections.push(action.payload.direction);
      }
      return { 
        ...state,
        directions: newDirections, 
      }
    }

    case 'DELETE_DIRECTION': {
      let newDirections = [...state.directions];
      newDirections = newDirections.filter((direction) => direction.country !== action.payload.direction.country);

      return { 
        ...state,
        directions: newDirections, 
      }

    }
    default: return state;
  }
}

export default reducer;
