const initState = {
  companyName: 'Dream',
  theme: 'orange',
  blocksFooter: [
    { id: 0, title: "Заголовок", text: "Образец текста нижнего колонтитула" },
    { id: 1, title: "Заголовок one", text: "Образец текста нижнего колонтитула" },
    // { id: 2, title: "Заголовок two", text: "Образец текста нижнего колонтитула" },		
  ],
}

function reducer(state = initState, action) {
  switch (action.type) {

    case 'ADD_COMPANY_NAME': {
      const { companyName } = action.payload; 
      return {
        ...state,
        companyName,
      }
    }

    case 'CHANGE_THEME': {
      const { theme } = action.payload;
      return {
        ...state,
        theme,
      }
    }

    // case 'ADD_BLOCK_FOOTER': {
    //   let newBlocksFooter = [...state.blocksFooter];
    //   let blockFooterIndex = newBlocksFooter.findIndex(blockFooter => blockFooter.title === action.payload.blockFooter.title);

    //   if (blockFooterIndex !== -1) {
    //     newBlocksFooter.splice(blockFooterIndex, 1, action.payload.blockFooter);       
    //   } else { 
    //     if (newBlocksFooter.length >= 3) {
    //       newBlocksFooter = newBlocksFooter.slice(0, -1);
    //     }
    //     newBlocksFooter.unshift(action.payload.blockFooter);        
    //   }

    //   return { 
    //     ...state,
    //     blocksFooter: newBlocksFooter, 
    //   }
    // }
    case 'ADD_BLOCK_FOOTER': {
      let newBlocksFooter = [...state.blocksFooter];

      if (newBlocksFooter.length === 3 ) {
        newBlocksFooter = newBlocksFooter.map((blockFooter) => {
          if (blockFooter.title.toLowerCase() === action.payload.blockFooter.title.toLowerCase()) {
            return action.payload.blockFooter;
          }
          return blockFooter;
        });
      } else {
        if (newBlocksFooter.length < 3) {
          newBlocksFooter = [...newBlocksFooter, action.payload.blockFooter];
        }
      }
      return { 
        ...state,
        blocksFooter: newBlocksFooter, 
      }
    }

    case 'DELETE_BLOCK_FOOTER': {
      let newBlocksFooter = [...state.blocksFooter];
      newBlocksFooter = newBlocksFooter.filter((blockFooter) => blockFooter.title.toLowerCase() !== action.payload.blockFooter.title.toLowerCase());
      return { 
        ...state,
        blocksFooter: newBlocksFooter, 
      }
    }

    default: return state;
  }
}

export default reducer;
