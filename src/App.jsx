import { Fragment } from 'react';
import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom';

import './App.scss';

import Footer from './layout/Footer/Footer';
import Header from './layout/Header/Header';
import HomePage from './pages/HomePage/HomePage';
import ContactsPage from './pages/ContactsPage/ContactsPage';
import Popup from './components/Popup/Popup';
import AdminPage from './pages/AdminPage/AdminPage';
import { fetchDirections } from './actionCreators/actionCreators';

class App extends PureComponent {
  componentDidMount() {
    const {fetchDirections} = this.props;
    fetchDirections();
  }

  render() {
    return (
      <Fragment>
        <Popup/>
        <Header />
        <main>
          <Switch>
            <Route path="/contacts/:id?">
              <ContactsPage />
            </Route>
            <Route exaсt path="/admin">
              <AdminPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </main>
        <Footer />
      </Fragment>
    );  
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchDirections: () => dispatch(fetchDirections()),
});

export default withRouter(connect(null, mapDispatchToProps)(App));