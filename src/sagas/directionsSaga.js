import { put, takeEvery } from 'redux-saga/effects';

import greece from '../../src/assets/img/DirectionsContainer/greece.jpeg';
import spain from '../../src/assets/img/DirectionsContainer/spain.jpeg';
import canada from '../../src/assets/img/DirectionsContainer/canada.jpeg';
import africa from '../../src/assets/img/DirectionsContainer/africa.jpeg';
import india from '../../src/assets/img/DirectionsContainer/india.jpg';
import vietnam from '../../src/assets/img/DirectionsContainer/vietnam.jpg';
import egypt from '../../src/assets/img/DirectionsContainer/egypt.jpg';

const delay = (ms) => new Promise ((res) => setTimeout(res, ms));

export function* fetchDirections(action) {
  yield put({ type: 'START_FETCH_DIRECTIONS' }); 
  yield delay(1000);

  const directions = [
    { id: 0, img: greece, country: 'Greece', title: "300 £ + Скидки", text: "От древних культур до удивительных пейзажей - найдите самые лучшие скидки" },
		{ id: 1, img: spain, country: 'Spain', title: "Праздники Испании", text: "Высокие горы, залитые солнцем побережья, мавританское наследие и более изысканная кухня" },
		{ id: 2, img: canada, country: 'Canada', title: "Велосипедные Каникулы", text: "Пробудите давно потерянное в детстве чувство свободы или бросьте вызов приключениям" },
		{ id: 3, img: africa, country: 'Africa', title: "Праздники Африки", text: "Экзотические базары, древние чудеса, уникальная дикая природа и огромные песчаные дюны в бесконечных пустынях." },
    { id: 4, img: india, country: 'India', title: "Туры в Гоа", text: "Древние храмы, великолепные пляжи, океан, на который можно смотреть бесконечно..." },
    { id: 8, img: vietnam, country: 'Vietnam', title: "Природные заповедники", text: "60 природных заповедников и много лесов, которые имеют культурное, историческое и экологическое значение." },
    { id: 9, img: egypt, country: 'Egypt', title: "Памятники истории", text: "Египетские пирамиды, потрясающие храмовые комплексы в Фивах, Луксоре и Дендере, древние христианские монастыри – лишь добавляют привлекательности поездке в Египет." }, 
  ];
  yield put ({type:'SUCCES_FETCH_DIRECTIONS', payload: {directions}});
}

export default function* directionsSaga() {
  yield takeEvery('FETCH_DIRECTIONS', fetchDirections);
}