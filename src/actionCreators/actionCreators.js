export function openPopup() {
  return {
    type: 'OPEN_POPUP',
  };
}

export function closePopup() {
  return {
    type: 'CLOSE_POPUP',
  };
}

export function fetchDirections() {
  return {
    type: 'FETCH_DIRECTIONS',
  };
}

export function addDirection(direction) {
  return {
    type: 'ADD_DIRECTION',
    payload: {direction},
  }
}

export function deleteDirection(direction) {
  return {
    type: 'DELETE_DIRECTION',
    payload: {direction},
  }
}

export function addCompanyName(companyName) {
  return {
    type: 'ADD_COMPANY_NAME',
    payload: {companyName},
  }
}

export function changeTheme(theme) {
  return {
    type: 'CHANGE_THEME',
    payload: {theme},
  }
}

export function addBlockFooter(blockFooter){
  return{
    type:'ADD_BLOCK_FOOTER',
    payload: {blockFooter},
  }
}

export function deleteBlockFooter(blockFooter) {
  return {
    type: 'DELETE_BLOCK_FOOTER',
    payload: {blockFooter},
  }
}
